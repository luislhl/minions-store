class OrdersController < ApplicationController
  protect_from_forgery with: :exception
  before_action :authenticate_user!

  def new
    @order = Order.new(product_id: params[:product])
  end

  def create
    @order = Order.new(order_params)

    if !@order.save
      render 'error'
    else
      OrderMailer.with(order: @order).new_order_email.deliver_later
    end
  end

  private

    def order_params
      params.require(:order).permit(
        :quantity, :product_id, :customer_name, :shipping_address)
    end
end

json.extract! product, :id, :description, :price, :imagePath, :discount, :created_at, :updated_at
json.url product_url(product, format: :json)

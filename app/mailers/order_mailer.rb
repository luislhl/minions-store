class OrderMailer < ApplicationMailer
  def new_order_email 
    @order = params[:order]

    to = ENV['NEW_ORDER_EMAIL_DESTINATION'] 
    mail(to: to, subject: 'New Order Created')
  end
end

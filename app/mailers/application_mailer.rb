class ApplicationMailer < ActionMailer::Base
  default from: 'minions@store.com'
  layout 'mailer'
end

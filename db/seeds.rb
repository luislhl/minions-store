# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Product.create(
  description: "Minion 1",
  price: 20.99,
  imagePath: "minion1.jpg",
  discount: 3
)

Product.create(
  description: "Minion 2",
  price: 27.99,
  imagePath: "minion2.jpg",
  discount: 4
)

Product.create(
  description: "Minion 3",
  price: 32.99,
  imagePath: "minion3.jpg",
  discount: 5
)

Product.create(
  description: "Minion 4",
  price: 37.99,
  imagePath: "minion4.jpg",
  discount: 6
)

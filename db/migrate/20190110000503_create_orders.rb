class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.datetime :created_at
      t.integer :product_id
      t.integer :user_id

      t.timestamps
    end
  end
end

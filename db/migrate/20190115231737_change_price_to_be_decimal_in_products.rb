class ChangePriceToBeDecimalInProducts < ActiveRecord::Migration[5.2]
  def change 
    reversible do |change|
      change.up { change_column :products, :price, :decimal }
      change.down { change_column :products, :price, :integer }
    end
  end
end

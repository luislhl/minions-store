class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :description
      t.integer :price
      t.string :imagePath
      t.integer :discount

      t.timestamps
    end
  end
end

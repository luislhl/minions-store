require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test "new order when user is not logged in should redirect to sign in" do
    get new_order_path
    assert_redirected_to(new_user_session_path)
  end

  test "new order when user is logged in should return the page" do
    sign_in User.create(email: "user@test.com",
      password: "foo", password_confirmation: "foo")

    get new_order_path
    assert_response :success
  end

end

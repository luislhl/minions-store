# Minions Store

https://enigmatic-sierra-14044.herokuapp.com/

This is a toy app made for learning purposes.

It allows users to authenticate and fill a form meant to be the reservation of a minion doll.
Each time the form is filled, an email is sent to a specified address.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

### Database 

As a first step, it's necessary that you install and configure a PostgreSQL database to use for development.

Install PostgreSQL. Search Google for instructions to your OS.

Create a user for development and test:

```
$ createuser minions_store --createdb
```

Next, create and migrate the database:

```
$ rails db:create
$ rails db:migrate
```

Finally, populate your local database with the data defined in `db/seeds.rb`:

```
$ rails db:seed
```

### Developing

Run the app in a local server:

```
$ rails server
```

Run the test suite to verify that everything is working correctly:

```
$ rails test
```

### Configuration 

We use the Figaro gem to help us set environment variables for use inside the app.

Its settings are located in the file `config/application.yml`.
Create the file if it does not exist yet.

You should set it like the following:

```yml
  GMAIL_USERNAME: <The username to use when sending emails through gmail>
  GMAIL_PASSWORD: <The gmail password>

  development:
    NEW_ORDER_EMAIL_DESTINATION: <Destination for New Order emails in Dev>
  production:
    NEW_ORDER_EMAIL_DESTINATION: <Destination for New Order emails in Prod>
```

### Deploy

You must have permissions to access the project in Heroku.

To deploy changes to Heroku, just push changes to the project git repository on Heroku:

```
$ git push heroku master
```

To update the Figaro environment variables on Heroku, run:

```
$ figaro heroku:set -e production
```

To check app logs in Heroku:

```
$ heroku logs --tail
```

## Future Improvements
Check in https://gitlab.com/luislhl/minions-store/issues
